# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
.PHONY: docs help test

# Use bash for inline if-statements in arch_patch target
SHELL:=bash
OWNER?=registry.gitlab.com/piersharding
NAME?=simple-jupyter
TAG?=0.0.9
IMG?=$(OWNER)/$(NAME):$(TAG)
CONTAINER_TOOL?=docker


# Disable BuildKit for Docker build
export DOCKER_BUILDKIT:=0

# https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:
	@echo "jupyter/docker-stacks"
	@echo "====================="
	@echo "Replace % with a stack directory name (e.g., make build/minimal-notebook)"
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the latest image for a stack using the system's architecture
	docker build $(DOCKER_BUILD_ARGS) --rm --force-rm --tag "$(IMG)" -f Dockerfile.base . --build-arg OWNER="$(OWNER)"
	@echo -n "Built image size: "
	@docker images "$(IMG)" --format "{{.Size}}"

push: ## push all tags for a jupyter image
	docker push --all-tags "$(OWNER)/$(NAME)"


build-test: ## build the latest image for a stack using the system's architecture
	docker build $(DOCKER_BUILD_ARGS) --rm --force-rm --tag "$(OWNER)/$(NAME)-test:$(TAG)" -f Dockerfile . --build-arg BASE_IMAGE="$(IMG)"
	@echo -n "Built image size: "
	@docker images "$(OWNER)/$(NAME)-test:$(TAG)" --format "{{.Size}}"


run-shell: ## run a bash in interactive mode in a stack
	docker run -it --rm "$(IMG)" $(SHELL)
run-sudo-shell: ## run a bash in interactive mode as root in a stack
	docker run -it --rm --user root "$(IMG)" $(SHELL)


# If you wish built the manager image targeting other platforms you can use the --platform flag.
# (i.e. docker build --platform linux/arm64 ). However, you must enable docker buildKit for it.
# More info: https://docs.docker.com/develop/develop-images/build_enhancements/
.PHONY: docker-build
docker-build: test ## Build docker image with the manager.
	$(CONTAINER_TOOL) build -t ${IMG}  -f Dockerfile.base .
	@echo -n "Built image size: "
	@docker images "$(IMG)" --format "{{.Size}}"

.PHONY: docker-push
docker-push: ## Push docker image with the manager.
	$(CONTAINER_TOOL) push ${IMG}

# PLATFORMS defines the target platforms for  the manager image be build to provide support to multiple
# architectures. (i.e. make docker-buildx IMG=myregistry/mypoperator:0.0.1). To use this option you need to:
# - able to use docker buildx . More info: https://docs.docker.com/build/buildx/
# - have enable BuildKit, More info: https://docs.docker.com/develop/develop-images/build_enhancements/
# - be able to push the image for your registry (i.e. if you do not inform a valid value via IMG=<myregistry/image:<tag>> then the export will fail)
# To properly provided solutions that supports more than one platform you should use this option.
PLATFORMS ?= linux/arm64,linux/amd64,linux/s390x,linux/ppc64le
.PHONY: docker-buildx
docker-buildx: test ## Build and push docker image for the manager for cross-platform support
	# copy existing Dockerfile and insert --platform=${BUILDPLATFORM} into Dockerfile.cross, and preserve the original Dockerfile
	sed -e '1 s/\(^FROM\)/FROM --platform=\$$\{BUILDPLATFORM\}/; t' -e ' 1,// s//FROM --platform=\$$\{BUILDPLATFORM\}/' Dockerfile > Dockerfile.cross
	- $(CONTAINER_TOOL) buildx create --name project-v3-builder
	$(CONTAINER_TOOL) buildx use project-v3-builder
	- $(CONTAINER_TOOL) buildx build --push --platform=$(PLATFORMS) --tag ${IMG} -f Dockerfile.cross .
	- $(CONTAINER_TOOL) buildx rm project-v3-builder
	rm Dockerfile.cross

.PHONY: rebuild
rebuild: docker-build docker-push
