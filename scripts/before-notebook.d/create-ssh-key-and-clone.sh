#!/usr/bin/env bash

set -ex

# check for the directory, not the file, as user may have added a differently-named key
if [ ! -d "${HOME}/.ssh" ] ; then
  mkdir -p "${HOME}/.ssh"
  chmod 0700 "${HOME}/.ssh"
  ssh-keygen -t ed25519 \
      -C "Low ITF JupyterLab key for ${JUPYTERHUB_USER}" \
      -q \
      -N "" \
      -f "${HOME}/.ssh/id_ed25519"
  ssh-keyscan -t ed25519 -H gitlab.com >> "${HOME}/.ssh/known_hosts"
fi

# Something gives group-write access to everything in $HOME on startup...fix it
grep -r '^-----BEGIN OPENSSH PRIVATE KEY-----' -l "${HOME}/.ssh" | xargs chmod -v 0600

#  check out the scripts repository
REPO_ROOT="${HOME}/ska-low-itf-scripts"
if [ ! -d "${REPO_ROOT}" ] ; then
    git clone https://gitlab.com/ska-telescope/aiv/ska-low-itf-scripts.git "${REPO_ROOT}"
    pushd "${REPO_ROOT}"
    git remote set-url origin git@gitlab.com:ska-telescope/aiv/ska-low-itf-scripts.git
    popd
fi

# The kernel won't work in Jupyter until this is run - presumably some setup happening
itango3 --debug -c "exit()"
