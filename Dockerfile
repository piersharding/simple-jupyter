# Copyright (c) Piers Harding.
# Distributed under the terms of the Apache2.0 License.
# ARG BASE_CONTAINER=artefact.skao.int/ska-tango-images-pytango-runtime:9.4.3
# FROM $BASE_CONTAINER
ARG BASE_IMAGE="registry.gitlab.com/piersharding/simple-jupyter:0.0.9"
FROM $BASE_IMAGE

ARG NB_USER=jovyan
ARG NB_UID=1001
ARG NB_GID=$NB_UID
LABEL maintainer="Piers Harding <piers@ompka.net>"

USER root

RUN cat /etc/passwd && echo "/home/${NB_USER}" && fix-permissions "/home/${NB_USER}"

# USER $NB_USER
USER ${NB_UID}

WORKDIR /home/$NB_USER

ENV HOME /home/$NB_USER

